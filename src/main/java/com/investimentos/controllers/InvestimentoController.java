package com.investimentos.controllers;

import com.investimentos.models.Investimento;
import com.investimentos.models.RequisicaoSimulacao;
import com.investimentos.models.RespostaSimulacao;
import com.investimentos.services.InvestimentoService;
import com.investimentos.services.RequisicaoSimulacaoService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    InvestimentoService investimentoService;
    @Autowired
    RequisicaoSimulacaoService requisicaoSimulacaoService;

    @PostMapping
    public ResponseEntity<Investimento> incluirInvestimento(@RequestBody @Valid Investimento investimento){

        Investimento investimentoObj = investimentoService.incluirInvestimento(investimento);
        return ResponseEntity.status(201).body(investimentoObj);
    }

    @GetMapping
    public Iterable<Investimento> buscarTodosInvestimentos(){
        return investimentoService.buscarTodosInvestimentos();
    }

    @GetMapping("/{id}")
    public Investimento buscarInvestimentopoId(@PathVariable Integer id){

        Optional<Investimento> investimentoOptional = investimentoService.buscarInvestimentoPotId(id);

        if (investimentoOptional.isPresent()) {
            return investimentoOptional.get();
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    public Investimento atualizarInvestimento(@PathVariable Integer id,@RequestBody @Valid Investimento investimentoAlt){
        Investimento investimentoData;
        investimentoAlt.setInvestimentoId(id);

        try {
            investimentoData = investimentoService.atualizarInvestimento(investimentoAlt);
            return investimentoData;
        }catch (ObjectNotFoundException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getLocalizedMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Investimento> deletarInvestimento(@PathVariable int id) {
        Optional<Investimento> investimentoOptional = investimentoService.buscarInvestimentoPotId(id);
        if (investimentoOptional.isPresent()){
            investimentoService.deletarInvestimento(investimentoOptional.get());
            return ResponseEntity.status(204).body(investimentoOptional.get());
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Não encontrado registro solicitado.");
        }
    }

    @PutMapping("/simulacao")
    public RespostaSimulacao simularInvestimento(@RequestBody @Valid RequisicaoSimulacao requisicaoSimulacao) {

        RespostaSimulacao respostaSimulacao= requisicaoSimulacaoService.simulacaoInvestimento(requisicaoSimulacao);
        return respostaSimulacao;

        }
}
