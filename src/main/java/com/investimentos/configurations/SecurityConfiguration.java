package com.investimentos.configurations;

import com.investimentos.security.FiltroAutenticacaoJWT;
import com.investimentos.security.FiltroAutorizacaoJWT;
import com.investimentos.security.JWTUtil;
import com.investimentos.services.UsuarioDetalhesService;
import com.investimentos.services.UsuarioService;
import com.sun.xml.internal.ws.api.model.CheckedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UsuarioDetalhesService usuarioDetalhesService;

    @Autowired
    private JWTUtil jwtUtil;

    private static final String[] PUBLIC_MATCHERS_GET = {
            "/investimentos"
    };
    private static final String[] PUBLIC_MATCHERS_POST = {
            "/investimentos",
            "/usuario/**"
    };

    private static final String[] PUBLIC_MATCHERS_PUT = {
            "/investimentos"
    };

    private static final String[] PUBLIC_MATCHERS_DELETE = {
            "/investimentos"
    };

    @Override
    public void configure(HttpSecurity http) throws Exception{
        http.cors();
        http.csrf().disable();

        http.authorizeRequests()
                .antMatchers(HttpMethod.GET,PUBLIC_MATCHERS_GET).permitAll()
                .antMatchers(HttpMethod.POST,PUBLIC_MATCHERS_POST).permitAll()
//                .antMatchers(HttpMethod.PUT,PUBLIC_MATCHERS_PUT).permitAll()
//                .antMatchers(HttpMethod.DELETE,PUBLIC_MATCHERS_DELETE).permitAll()
                .anyRequest().authenticated();

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilter(new FiltroAutenticacaoJWT(jwtUtil, authenticationManager()));
        http.addFilter(new FiltroAutorizacaoJWT(authenticationManager(),usuarioDetalhesService , jwtUtil));
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    };
}
