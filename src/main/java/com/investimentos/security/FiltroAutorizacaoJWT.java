package com.investimentos.security;

import com.investimentos.services.UsuarioDetalhesService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FiltroAutorizacaoJWT extends BasicAuthenticationFilter {

    private UsuarioDetalhesService usuarioDetalhesService;
    private JWTUtil jwtUtil;

    public FiltroAutorizacaoJWT(AuthenticationManager authenticationManager, UsuarioDetalhesService usuarioDetalhesService, JWTUtil jwtUtil) {
        super(authenticationManager);
        this.jwtUtil = jwtUtil;
        this.usuarioDetalhesService = usuarioDetalhesService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {
        String tokenHeader = request.getHeader("Authorization");
        if (tokenHeader != null && tokenHeader.startsWith("Bearer ")){
            UsernamePasswordAuthenticationToken auth = getAuthentication(request,tokenHeader.substring(7));
            if (auth != null ){
                SecurityContextHolder.getContext().setAuthentication(auth);
            }
        }
        chain.doFilter(request,response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request, String token) {
        if (jwtUtil.tokenValido(token)){
            String userName = jwtUtil.getUserName(token);
            UserDetails user = usuarioDetalhesService.loadUserByUsername(userName);
            return new UsernamePasswordAuthenticationToken(user,null,user.getAuthorities());
        }
        return null;
    }}
