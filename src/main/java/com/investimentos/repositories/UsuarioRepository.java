package com.investimentos.repositories;

import com.investimentos.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

    //Executa a query no banco de dados para consulta por email.
    Usuario findByEmail( String email);

}
