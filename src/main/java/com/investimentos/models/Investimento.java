package com.investimentos.models;

import com.investimentos.enums.RiscoDoInvestimento;
import javax.validation.*;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int investimentoId;
    @Column(name ="nome_investimento")
    @NotNull
    @Size(min = 1, max = 100, message = "O nome do investimento deve ser preenchido")
    private String nomeInvestimento;
    @NotNull(message = "Descrição do investimento deve ser preenchido.")
    private String descricao;
    @Column(name ="risco_investimento")
    @NotNull(message = "Risco do investimento deve ser preenchido.")
    private RiscoDoInvestimento riscoDoInvestimento;
    @Column(name ="porcentagem_lucro")
    @NotNull(message = "Percentual de lucro deve ser preenchido.")
    @DecimalMin(value = "0.1", message = "Percentual de lucro não pode ser zero.")
    private Double porcentagemDeLucro;

    public Investimento() {
    }

    public int getInvestimentoId() {
        return investimentoId;
    }

    public void setInvestimentoId(int investimentoId) {
        this.investimentoId = investimentoId;
    }

    public String getNomeInvestimento() {
        return nomeInvestimento;
    }

    public void setNomeInvestimento(String nomeInvestimento) {
        this.nomeInvestimento = nomeInvestimento;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public RiscoDoInvestimento getRiscoDoInvestimento() {
        return riscoDoInvestimento;
    }

    public void setRiscoDoInvestimento(RiscoDoInvestimento riscoDoInvestimento) {
        this.riscoDoInvestimento = riscoDoInvestimento;
    }

    public Double getPorcentagemDeLucro() {
        return porcentagemDeLucro;
    }

    public void setPorcentagemDeLucro(Double porcentagemDeLucro) {
        this.porcentagemDeLucro = porcentagemDeLucro;
    }
}
