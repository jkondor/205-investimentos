package com.investimentos.models;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class RequisicaoSimulacao {
    @NotNull(message = "Identificador do investimento deve ser preenchido.")
    private  int investimentoId;
    @NotNull(message = "Quantidade de meses de aplicação deve ser preenchido.")
    private  int mesesDeAplicacao;
    @NotNull(message = "Quantidade de dinheiro para aplicação deve ser preenchido.")
    @DecimalMin("100.0")
    private  Double dinheiroAplicado;

    public RequisicaoSimulacao() {
    }

    public int getInvestimentoId() {
        return investimentoId;
    }

    public void setInvestimentoId(int investimentoId) {
        this.investimentoId = investimentoId;
    }

    public int getMesesDeAplicacao() {
        return mesesDeAplicacao;
    }

    public void setMesesDeAplicacao(int mesesDeAplicacao) {
        this.mesesDeAplicacao = mesesDeAplicacao;
    }

    public Double getDinheiroAplicado() {
        return dinheiroAplicado;
    }

    public void setDinheiroAplicado(Double dinheiroAplicado) {
        this.dinheiroAplicado = dinheiroAplicado;
    }
}
