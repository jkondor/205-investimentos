package com.investimentos.models;

public class RespostaSimulacao {
    private double resultadoSimulacao;

    public RespostaSimulacao() {
    }

    public double getResultadoSimulacao() {
        return resultadoSimulacao;
    }

    public void setResultadoSimulacao(double resultadoSimulacao) {
        this.resultadoSimulacao = resultadoSimulacao;
    }
}
