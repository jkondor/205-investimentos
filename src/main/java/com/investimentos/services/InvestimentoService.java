package com.investimentos.services;

import com.investimentos.models.Investimento;
import com.investimentos.models.RequisicaoSimulacao;
import com.investimentos.models.RespostaSimulacao;
import com.investimentos.repositories.InvestimentoRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    InvestimentoRepository investimentoRepository;

    public Iterable<Investimento> buscarTodosInvestimentos() {
        Iterable<Investimento> todosInvestimentos = investimentoRepository.findAll();
        return todosInvestimentos;
    }

    public Optional<Investimento> buscarInvestimentoPotId(int id) {
        Optional<Investimento> investimentoOptional = investimentoRepository.findById(id);
        return investimentoOptional;
    }


    public Investimento incluirInvestimento(Investimento investimento) {
        Investimento investimentoObj = investimentoRepository.save(investimento);
        return investimentoObj;
    }


    public Investimento atualizarInvestimento(Investimento investimento) {
        Optional<Investimento> investimentoOptional = buscarInvestimentoPotId(investimento.getInvestimentoId());
        if (investimentoOptional.isPresent()) {
            Investimento investimentoObjeto = investimentoRepository.save(investimento);
            return investimentoObjeto;
        }else{
            throw new ObjectNotFoundException(Investimento.class,"O investimento não foi encontrado.");
        }
    }

    public void deletarInvestimento(Investimento investimento) {
        investimentoRepository.delete(investimento);
    }

}
