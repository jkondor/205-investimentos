package com.investimentos.services;

import com.investimentos.models.Usuario;
import com.investimentos.repositories.UsuarioRepository;
import com.investimentos.security.DetalhesUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService  {

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public Usuario salvarUsuario(Usuario usuario){
        Usuario usuarioOBJ = usuarioRepository.findByEmail(usuario.getEmail());

        if (usuarioOBJ != null){
            throw new RuntimeException("Esse email já existe");
        }
        String senha = usuario.getSenha();
        String encode = bCryptPasswordEncoder.encode(senha);
        usuario.setSenha(encode);

        return usuarioRepository.save(usuario);
    }

}
