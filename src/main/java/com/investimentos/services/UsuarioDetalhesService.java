package com.investimentos.services;

import com.investimentos.models.Usuario;
import com.investimentos.repositories.UsuarioRepository;
import com.investimentos.security.DetalhesUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UsuarioDetalhesService implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Usuario usuario = usuarioRepository.findByEmail(email);
        if (usuario==null){
            throw new UsernameNotFoundException(email);
        }

        DetalhesUsuario userDetails = new DetalhesUsuario(usuario.getId(),usuario.getEmail(),usuario.getSenha());
        return userDetails;
    }
}