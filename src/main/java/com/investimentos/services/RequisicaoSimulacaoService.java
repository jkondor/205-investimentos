package com.investimentos.services;

import com.investimentos.models.Investimento;
import com.investimentos.models.RequisicaoSimulacao;
import com.investimentos.models.RespostaSimulacao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.Optional;

@Service
public class RequisicaoSimulacaoService {

    @Autowired
    InvestimentoService investimentoService;

    public RespostaSimulacao simulacaoInvestimento(RequisicaoSimulacao requisicaoSimulacao){

        RespostaSimulacao respostaSimulacao = new RespostaSimulacao();

        Optional<Investimento> investimentoOptional =  investimentoService.buscarInvestimentoPotId(requisicaoSimulacao.getInvestimentoId());

        Investimento investimento = investimentoOptional.get();

        Double jurosTotal = Math.pow((1 + investimento.getPorcentagemDeLucro()/100), requisicaoSimulacao.getMesesDeAplicacao());

        // Calcula valor com juros e formata com 2 casas decimais.
        DecimalFormat df = new DecimalFormat("##########.00");
        String Str =  df.format(requisicaoSimulacao.getDinheiroAplicado() * jurosTotal);
        Str = Str.replaceAll(",",".");

        respostaSimulacao.setResultadoSimulacao(Double.parseDouble(Str));

        return respostaSimulacao;
    }
}
