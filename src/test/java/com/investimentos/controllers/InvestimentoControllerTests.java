package com.investimentos.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.investimentos.enums.RiscoDoInvestimento;
import com.investimentos.models.Investimento;
import com.investimentos.models.RequisicaoSimulacao;
import com.investimentos.models.RespostaSimulacao;
import com.investimentos.services.InvestimentoService;
import com.investimentos.services.RequisicaoSimulacaoService;
import org.hamcrest.CoreMatchers;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(InvestimentoController.class)
public class InvestimentoControllerTests {
    @MockBean
    InvestimentoService investimentoService;
    @MockBean
    RequisicaoSimulacaoService requisicaoSimulacaoService;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    Investimento investimento;

    @BeforeEach
    public void inicializar(){
        investimento = new Investimento();
        investimento.setInvestimentoId(1);
        investimento.setNomeInvestimento("Renda Fixa");
        investimento.setDescricao("Aplicação em RF");
        investimento.setRiscoDoInvestimento(RiscoDoInvestimento.BAIXO);
        investimento.setPorcentagemDeLucro(0.5);
    }

    @Test
    public void testarIncluirInvestimento() throws Exception {

        Mockito.when(investimentoService.incluirInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.investimentoId", CoreMatchers.equalTo(1)));

    }

    @Test
    public void testarBuscarTodosInvestimentos() throws Exception {

        Iterable<Investimento> investimentoIterable = Arrays.asList(investimento);

        //String json = mapper.writeValueAsString(investimento);

        Mockito.when(investimentoService.buscarTodosInvestimentos()).thenReturn(investimentoIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos")
        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].investimentoId",CoreMatchers.equalTo(investimento.getInvestimentoId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].nomeInvestimento",CoreMatchers.equalTo(investimento.getNomeInvestimento())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].descricao",CoreMatchers.equalTo(investimento.getDescricao())));
    }

    @Test
    public void testarNaoEncontrarBuscaTodosInvestimento() throws Exception {
        Iterable<Investimento> investimentoIterable = Arrays.asList();

        Mockito.when(investimentoService.buscarTodosInvestimentos()).thenReturn(investimentoIterable);

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$" ).isEmpty());
    }

    @Test
    public void testarBuscarInvestimentopoId() throws Exception {
        Optional<Investimento> investimentoOptional = Optional.of(investimento);

        Mockito.when(investimentoService.buscarInvestimentoPotId(Mockito.anyInt())).thenReturn(investimentoOptional);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/1")
                    .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.investimentoId",CoreMatchers.equalTo(investimento.getInvestimentoId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nomeInvestimento",CoreMatchers.equalTo(investimento.getNomeInvestimento())));
    }

    @Test
    public void testarNaoEncontrarBuscarInvestimentopoId() throws Exception {
        Optional<Investimento> investimentoOptional = Optional.empty();

        Mockito.when(investimentoService.buscarInvestimentoPotId(Mockito.anyInt())).thenReturn(investimentoOptional);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$" ).doesNotExist());
    }

    @Test
    public void testarAtualizarInvestimento() throws Exception {

        Mockito.when(investimentoService.atualizarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.investimentoId",CoreMatchers.equalTo(investimento.getInvestimentoId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nomeInvestimento",CoreMatchers.equalTo(investimento.getNomeInvestimento())));

    }

    @Test
    public void testarPercentualLucroZeroAtualizarInvestimento() throws Exception {

        Mockito.when(investimentoService.atualizarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);

        investimento.setPorcentagemDeLucro(0.0);
        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }

    @Test
    public void testarNaoEncontrarAtualizarInvestimento() throws Exception {

        Optional<Investimento> investimentoOptional = Optional.empty();

        Mockito.when(investimentoService.buscarInvestimentoPotId(Mockito.anyInt())).thenReturn(investimentoOptional);

        Mockito.when(investimentoService.atualizarInvestimento(Mockito.any(Investimento.class))).thenThrow(new ObjectNotFoundException(Investimento.class,"O investimento não foi encontrado."));

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }

    @Test
    public void testarDeletarInvestimento() throws Exception {

        Optional<Investimento> investimentoOptional = Optional.of(investimento);

        Mockito.when(investimentoService.buscarInvestimentoPotId(Mockito.anyInt())).thenReturn(investimentoOptional);
        Mockito.doNothing().when(investimentoService).deletarInvestimento(Mockito.any(Investimento.class));

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isNoContent())
                .andExpect(MockMvcResultMatchers.jsonPath("$.investimentoId", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarNaoEncontrarDeletarInvestimento() throws Exception {

        Optional<Investimento> investimentoOptional = Optional.empty();

        Mockito.when(investimentoService.buscarInvestimentoPotId(Mockito.anyInt())).thenReturn(investimentoOptional);
        Mockito.doNothing().when(investimentoService).deletarInvestimento(Mockito.any(Investimento.class));

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }

    @Test
    public void testarSolicitarSimulacao() throws Exception {

        RequisicaoSimulacao requisicaoSimulacao = new RequisicaoSimulacao();
        requisicaoSimulacao.setInvestimentoId(4);
        requisicaoSimulacao.setDinheiroAplicado(1000.00);
        requisicaoSimulacao.setMesesDeAplicacao(10);

        RespostaSimulacao respostaSimulacao = new RespostaSimulacao();
        respostaSimulacao.setResultadoSimulacao(1015.14);

        Mockito.when(requisicaoSimulacaoService.simulacaoInvestimento(Mockito.any(RequisicaoSimulacao.class))).thenReturn(respostaSimulacao);

        String json = mapper.writeValueAsString(requisicaoSimulacao);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.resultadoSimulacao", CoreMatchers.equalTo(1015.14)));
    }
}
