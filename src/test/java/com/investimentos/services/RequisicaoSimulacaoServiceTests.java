package com.investimentos.services;

import com.investimentos.enums.RiscoDoInvestimento;
import com.investimentos.models.Investimento;
import com.investimentos.models.RequisicaoSimulacao;
import com.investimentos.models.RespostaSimulacao;
import com.investimentos.repositories.InvestimentoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class RequisicaoSimulacaoServiceTests {

    @MockBean
    InvestimentoService investimentoService;

    @Autowired
    RequisicaoSimulacaoService requisicaoSimulacaoService;

    RequisicaoSimulacao requisicaoSimulacao;

    @BeforeEach
    public void inicilizar(){
        requisicaoSimulacao = new RequisicaoSimulacao();
        requisicaoSimulacao.setInvestimentoId(1);
        requisicaoSimulacao.setDinheiroAplicado(1000.00);
        requisicaoSimulacao.setMesesDeAplicacao(10);

    }

    @Test
    public void testarSimularInvestimento(){

        Investimento investimento = new Investimento();
        investimento.setInvestimentoId(2);
        investimento.setNomeInvestimento("Poupanca");
        investimento.setDescricao("Aplicação em poupanca");
        investimento.setRiscoDoInvestimento(RiscoDoInvestimento.BAIXO);
        investimento.setPorcentagemDeLucro(0.5);

        Optional<Investimento> investimentoOptional = Optional.of(investimento);

        Mockito.when(investimentoService.buscarInvestimentoPotId(Mockito.anyInt())).thenReturn(investimentoOptional);

        RespostaSimulacao respostaSimulacao =  requisicaoSimulacaoService.simulacaoInvestimento(requisicaoSimulacao);
        Assertions.assertEquals(1051.14,respostaSimulacao.getResultadoSimulacao());
    }
}
