package com.investimentos.services;

import com.investimentos.enums.RiscoDoInvestimento;
import com.investimentos.models.Investimento;
import com.investimentos.repositories.InvestimentoRepository;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class InvestimentoServiceTests {

    @MockBean
    InvestimentoRepository investimentoRepository;

    @Autowired
    InvestimentoService investimentoService;

    Investimento investimento;

    @BeforeEach
    public void inicializar(){
        investimento = new Investimento();
        investimento.setInvestimentoId(1);
        investimento.setNomeInvestimento("Renda Fixa");
        investimento.setDescricao("Aplicação em RF");
        investimento.setRiscoDoInvestimento(RiscoDoInvestimento.BAIXO);
        investimento.setPorcentagemDeLucro(0.65);

    }

    @Test
    public void testarBuscarTodosInvestimentos(){

        Iterable<Investimento> investimentoIterable = Arrays.asList(investimento);

        Mockito.when(investimentoRepository.findAll()).thenReturn(investimentoIterable);

        Iterable<Investimento> investimentoObjeto = investimentoService.buscarTodosInvestimentos();

        Assertions.assertEquals(investimentoIterable,investimentoObjeto);
    }

    @Test
    public void testarBuscarPorId(){

        Optional<Investimento> investimentoOptional = Optional.of(investimento);
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(investimentoOptional);

        Optional<Investimento> investimentoObjeto = investimentoService.buscarInvestimentoPotId(1);

        Assertions.assertEquals(investimentoObjeto, investimentoOptional);
        Mockito.verify(investimentoRepository, Mockito.times(1)).findById(Mockito.any(int.class));

    }

    @Test
    public void testarSalvarInvestimento(){

        Mockito.when(investimentoRepository.save( Mockito.any(Investimento.class))).thenReturn(investimento);
        Investimento investimetoObjeto = investimentoService.incluirInvestimento(investimento);
        Assertions.assertEquals(investimento, investimetoObjeto);

    }

    @Test
    public void testarAtualizarInvestimento(){

        Optional<Investimento> investimentoOptional = Optional.of(investimento);
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(investimentoOptional);
        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimento);

        Investimento investimentoObjeto = investimentoService.atualizarInvestimento(investimento);

        Assertions.assertEquals(investimentoObjeto, investimento);
    }

    @Test
    public void testarNaoEncontrouAtualizarInvestimento(){

        Investimento investimento1 = new Investimento();
        investimento1.setInvestimentoId(2);
        investimento1.setNomeInvestimento("Poupanca");
        investimento1.setDescricao("Aplicação em poupanca");
        investimento1.setRiscoDoInvestimento(RiscoDoInvestimento.BAIXO);
        investimento1.setPorcentagemDeLucro(0.15);

        Optional<Investimento> investimentoOptional = Optional.empty();
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(investimentoOptional);
        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimento1);

        Assertions.assertThrows(ObjectNotFoundException.class,() -> {investimentoService.atualizarInvestimento(investimento);});
    }

    @Test
    public void testarDeletarInvestimento(){
        investimentoService.deletarInvestimento(investimento);
        Mockito.verify(investimentoRepository, Mockito.times(1)).delete(Mockito.any(Investimento.class));
    }
}
